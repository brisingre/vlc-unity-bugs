using UnityEngine;
using System;
using LibVLCSharp;
using UnityEngine.Video;
using UnityEngine.UI;
using System.IO;
using System.Collections;

/// this class serves as an example on how to configure playback in Unity with VLC for Unity using LibVLCSharp.
/// for libvlcsharp usage documentation, please visit https://code.videolan.org/videolan/LibVLCSharp/-/blob/master/docs/home.md
public class BufferSizeStuck : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    Texture2D tex = null;
    bool playing;

    public string tallVideoName;
    public string squareVideoName;
    public string wideVideoName;
    public Text statusMessage;



    void Awake()
    {
        Core.Initialize(Application.dataPath);

        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        //_libVLC.Log += (s, e) => UnityEngine.Debug.Log(e.FormattedLog); // enable this for logs in the editor

        _mediaPlayer = new MediaPlayer(_libVLC);

        StartCoroutine(TestCOR()); //Start the test
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    void Update()
    {
        //Get size every frame
        uint i_videoHeight = 0;
        uint i_videoWidth = 0;
        _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);

        //Automatically resize output texture (tex) if Size changes
        if (tex == null || tex.width != i_videoWidth || tex.height != i_videoHeight)
        {
            ResizeOutputTexture(i_videoWidth, i_videoHeight);
            ResizeQuadToVideoSize(i_videoWidth, i_videoHeight);
        }
        else if (tex != null)
        {
            //Update the output texture (tex)
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (updated)
            {
                tex.UpdateExternalTexture(texptr);
            }
        }


    }

    //Play a video
    public void Play(string path)
    {
        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        _mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        _mediaPlayer.Play();

    }

    //Get a path to a video in the StreamingAssets folder
    string GetStreamingAssetsPath(string filename)
    {
        return Application.streamingAssetsPath + "/" + filename;

    }

    //This resizes the output texture (tex) to the size VLC thinks the video is
    void ResizeOutputTexture(uint px, uint py)
    {
        var texptr = _mediaPlayer.GetTexture(px, py, out bool updated);
        if (px != 0 && py != 0 && updated && texptr != IntPtr.Zero)
        {
            Debug.Log("Creating texture with height " + py + " and width " + px);
            tex = Texture2D.CreateExternalTexture((int)px,
                (int)py,
                TextureFormat.RGBA32,
                false,
                true,
                texptr);
            GetComponent<Renderer>().material.mainTexture = tex;
        }
    }

    //This stretches the 3d output quad to the same aspect ratio as the output texture (tex)
    //Without this, the output texture will be stretched into a square, which hides the bug
    void ResizeQuadToVideoSize(uint px, uint py)
    {
        var height = 1;
        var width = height * ((float)px / (float)py);
        if (width > 0 && height > 0)
            transform.localScale = new Vector3(width, -height, 1);
    }

    //Sets the status message and logs to the console
    void ShowMessage(string message)
    {
        Debug.Log(message);
        statusMessage.text = message;
    }

    //Play our videos one at a time every 4 seconds
    IEnumerator TestCOR()
    {
        for (int i = 0; i < 500; i++)
        {
            //Make sure we have a fresh Media Player so the bug won't appear until we want it to
            _mediaPlayer = new MediaPlayer(_libVLC);

            //Play videos quickly
            ShowMessage("As long as we switch before the videos finish, everything resizes like it should.");

            for (int j = 0; j < 3; j++)
            {
                Play(GetStreamingAssetsPath(wideVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(tallVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(squareVideoName));
                yield return new WaitForSeconds(1);
            }


            //Get stuck on Wide
            ShowMessage("If we let a video finish, playing new videos won't automatically resize whatever internal buffer they're drawn to in VLC anymore. " +
                "Size still returns the right value, and we can still resize the Unity textures.");
            Play(GetStreamingAssetsPath(wideVideoName));
            yield return new WaitForSeconds(12);


            ShowMessage("Stuck on wide!!! Until we make a new video player, everything we play will be letterboxed into a 1000w by 500w buffer in VLC. ");
            for (int j = 0; j < 3; j++)
            {
                Play(GetStreamingAssetsPath(wideVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(tallVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(squareVideoName));
                yield return new WaitForSeconds(1);
            }

            //Get unstuck
            ShowMessage("Making a new MediaPlayer fixes the issue. This is the only way I know of to fix it.");
            _mediaPlayer = new MediaPlayer(_libVLC);
            for (int j = 0; j < 3; j++)
            {
                Play(GetStreamingAssetsPath(wideVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(tallVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(squareVideoName));
                yield return new WaitForSeconds(1);
            }


            //Get stuck on Tall
            ShowMessage("If we let this video finish we'll get stuck on Tall. These videos are about 10 seconds long. I'm sorry they don't move, I know that's confusing.");
            Play(GetStreamingAssetsPath(tallVideoName));
            yield return new WaitForSeconds(12);

            ShowMessage("Stuck on tall!!! Until we make a new video player, everything we play will be letterboxed into a 500w by 1000w buffer in VLC.");
            for (int j = 0; j < 3; j++)
            {
                Play(GetStreamingAssetsPath(wideVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(tallVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(squareVideoName));
                yield return new WaitForSeconds(1);
            }

            //Get stuck on Square
            ShowMessage("Resetting the media player and getting stuck on Square.");
            _mediaPlayer = new MediaPlayer(_libVLC);
            Play(GetStreamingAssetsPath(squareVideoName));
            yield return new WaitForSeconds(12);

            ShowMessage("Stuck on square!!! Until we make a new video player, everything we play will be letterboxed into a 1000w by 1000w buffer in VLC. ");
            for (int j = 0; j < 3; j++)
            {
                Play(GetStreamingAssetsPath(wideVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(tallVideoName));
                yield return new WaitForSeconds(1);
                Play(GetStreamingAssetsPath(squareVideoName));
                yield return new WaitForSeconds(1);
            }


        }
    }




}
