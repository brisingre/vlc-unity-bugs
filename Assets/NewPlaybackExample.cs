using UnityEngine;
using System;
using LibVLCSharp;

/// This is a heavily modified version of MinimalPlayback.cs. It is no longer minimal
/// It includes workarounds for several issues and some other quality-of-life improvements
/// -- A public path variable so you can use your own videos (Quality-of-life)
/// -- A public bool to toggle logging (Quality-of-life)
/// -- A public screen variable so you can set what object renders your video
/// -- A public texture variable you can refer to from other scripts, to get the output texture of the player
/// -- A public bool to toggle creating a new MediaPlayer for each new video. This is no longer necessary and should be removed.
/// -- A seperate texture for VLC to render into (_vlctex) and for Unity to use (texture). This works around issues #112 and #118
/// -- A check on the orientation of the currently-playing video when we resize the textures. This works around issue #113
/// -- Public bools for whether to flip the texture, which we can automatically set false on Android.

public class NewPlaybackExample : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    Texture2D _vlcTex = null; //This is the texture libVLC writes to directly. It's private.

    public RenderTexture texture = null; //We copy it into this texture which we actually use in unity. It's public.

    public Renderer screen;

    public string path = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

    public long seekTimeDelta = 5000;

    public bool enableLogging = false;
    public bool alwaysRecreateMediaPlayer = true; //This should be fixed soon, but right now this is needed to avoid an issue with video sizes getting stuck. It might avoid other issues in the future.

    public bool FlipTextureX = false; //No particular reason you'd need this but it's useful
    public bool FlipTextureY = true; //Set to false on Android platform

    void Awake()
    {
        //Setup LibVLC
        Core.Initialize(Application.dataPath);
        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");


        //Setup Error Logging
        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        _libVLC.Log += (s, e) =>
        {
            if(enableLogging)
                Debug.Log(e.FormattedLog); 
        };

        //Setup Screen
        if (screen == null)
            screen = GetComponent<Renderer>();

        //Setup Media Player
        CreateMediaPlayer();
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    void Update()
    {
        //Get size every frame
        uint i_videoHeight = 0;
        uint i_videoWidth = 0;
        _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);

        //Automatically resize output textures if size changes
        if (_vlcTex == null || _vlcTex.width != i_videoWidth || _vlcTex.height != i_videoHeight)
        {
            ResizeOutputTextures(i_videoWidth, i_videoHeight);
        }


        if (_vlcTex != null)
        {
            //Update the vlc texture (tex)
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (updated)
            {
                _vlcTex.UpdateExternalTexture(texptr);

                //Copy the vlc texture into the output texture, flipped over
                var flip = new Vector2(FlipTextureX ? -1 : 1, FlipTextureY ? -1 : 1);
                Graphics.Blit(_vlcTex, texture, flip, Vector2.zero); //If you wanted to do post processing outside of VLC you could use a shader here.
            }
        }
    }

    //Right now these are designed to be drop-in replacements for MinimalPlayback.cs. In the long run that will probably change.
	#region controls
	//Play
	public void Play(string path)
    {
        this.path = path; 
        Play();
    }

    public void Play()
    {
        Debug.Log("NewPlaybackExample Play");
        if (alwaysRecreateMediaPlayer)
            CreateMediaPlayer();

        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        _mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        _mediaPlayer.Play();
    }

    //Pause
    public void Pause()
    {
        Debug.Log("NewPlaybackExample Pause");
        _mediaPlayer.Pause();
	}

    public void PlayPause()
    {
        Debug.Log("NewPlaybackExample PlayPause");
        if (_mediaPlayer.IsPlaying)
		{
            Pause();
		}
        else
		{
            Play();
		}
	}


    //Stop
    public void Stop()
	{
        Debug.Log("NewPlaybackExample Stop");
        _mediaPlayer?.Stop();

        // there is no need to dispose every time you stop, but you should do so when you're done using the mediaplayer and this is how:
        // _mediaPlayer?.Dispose(); 
        // _mediaPlayer = null;

        screen.material.mainTexture = null;
        _vlcTex = null;
        texture = null;
    }


    //Seek
    public void Seek(long timeDelta)
    {
        Debug.Log("NewPlaybackExample Seek " + timeDelta);
        _mediaPlayer.SetTime(_mediaPlayer.Time + timeDelta);
    }

    public void SeekForward()
	{
        Seek(seekTimeDelta);
	}

    public void SeekBackward()
	{
        Seek(-seekTimeDelta);
	}

	#endregion


	#region internal
	//This makes a new MediaPlayer object and disposes of the old one
	void CreateMediaPlayer()
    {
        if(_mediaPlayer != null)
		{
            _mediaPlayer?.Dispose();
            _mediaPlayer = null;
        }
        _mediaPlayer = new MediaPlayer(_libVLC);

    }

    //This resizes the output textures to the size VLC thinks the video is
    void ResizeOutputTextures(uint px, uint py)
    {
        var texptr = _mediaPlayer.GetTexture(px, py, out bool updated);
        if (px != 0 && py != 0 && updated && texptr != IntPtr.Zero)
        {
            //If the currently playing video uses the Bottom Right orientation, we have to do this to avoid stretching it.
            if(GetVideoOrientation() == VideoOrientation.BottomRight)
            {
                uint swap = px;
                px = py;
                py = swap;
			}

            _vlcTex = Texture2D.CreateExternalTexture((int)px, (int)py, TextureFormat.RGBA32, false, true, texptr); //Make a texture of the proper size for the video to output to
            texture = new RenderTexture(_vlcTex.width, _vlcTex.height, 0, RenderTextureFormat.ARGB32); //Make a renderTexture the same size as vlctex

            if(screen != null)
                screen.material.mainTexture = texture;
        }
    }

    //This returns the video orientation for the currently playing video, if there is one
    VideoOrientation? GetVideoOrientation()
	{
        var tracks = _mediaPlayer?.Tracks(TrackType.Video);

        if (tracks == null || tracks.Count == 0)
            return null;

        var orientation = tracks[0]?.Data.Video.Orientation; //At the moment we're assuming the track we're playing is the first track

        return orientation;
    }
	#endregion

}
