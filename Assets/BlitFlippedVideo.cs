using UnityEngine;
using System;
using LibVLCSharp;
using UnityEngine.UI;
using System.Collections;

/// this class serves as an example on how to configure playback in Unity with VLC for Unity using LibVLCSharp.
/// for libvlcsharp usage documentation, please visit https://code.videolan.org/videolan/LibVLCSharp/-/blob/master/docs/home.md
public class BlitFlippedVideo : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    Texture2D vlcTex = null; //This is the texture libVLC writes to directly. It's private.
    
    public RenderTexture outputTex = null; //We copy it into this texture which we actually use in unity. It's public.


    public string testVideoName;
    public Text statusMessage;


    public MeshRenderer brokenScreen;
    public MeshRenderer fixedScreen;


    void Awake()
    {
        Core.Initialize(Application.dataPath);

        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        //_libVLC.Log += (s, e) => UnityEngine.Debug.Log(e.FormattedLog); // enable this for logs in the editor

        _mediaPlayer = new MediaPlayer(_libVLC);

        StartCoroutine(TestCOR());
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    void Update()
    {
        //Get size every frame
        uint i_videoHeight = 0;
        uint i_videoWidth = 0;
        _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);

        //Automatically resize output texture (tex) if Size changes
        if (vlcTex == null || vlcTex.width != i_videoWidth || vlcTex.height != i_videoHeight)
        {
            ResizeOutputTexture(i_videoWidth, i_videoHeight);
        }
        

        if (vlcTex != null)
        {
            //Update the output texture (tex)
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (updated)
            {
                vlcTex.UpdateExternalTexture(texptr);
                Graphics.Blit(vlcTex, outputTex, new Vector2(-1, 1), Vector2.zero); //Copy the vlc texture into the output texture, flipped.
            }
        }
    }

    //Play a video
    public void Play(string path)
    {
        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        _mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        _mediaPlayer.Play();

    }

    //Get a path to a video in the StreamingAssets folder
    string GetStreamingAssetsPath(string filename)
    {
        return Application.streamingAssetsPath + "/" + filename;

    }

    //This resizes the output texture (tex) to the size VLC thinks the video is
    void ResizeOutputTexture(uint px, uint py)
    {
        var texptr = _mediaPlayer.GetTexture(px, py, out bool updated);
        if (px != 0 && py != 0 && updated && texptr != IntPtr.Zero)
        {
            Debug.Log("Creating texture with height " + py + " and width " + px);

            vlcTex = Texture2D.CreateExternalTexture((int)px, (int)py, TextureFormat.RGBA32, false, true, texptr);
            outputTex = new RenderTexture(vlcTex.width, vlcTex.height, 0, RenderTextureFormat.ARGB32); //Make a renderTexture the same size as vlctex

            brokenScreen.material.mainTexture = vlcTex;
            fixedScreen.material.mainTexture = outputTex;

        }
    }


    //Just loop one video for this one
    IEnumerator TestCOR()
    {
        for (int i = 0; i < 500; i++)
        {

            Play(GetStreamingAssetsPath(testVideoName));
            yield return new WaitForSeconds(9); //This should restart a little before the end of the video, that isn't really important

        }

    }
}
