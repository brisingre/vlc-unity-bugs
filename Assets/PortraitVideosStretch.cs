using UnityEngine;
using System;
using LibVLCSharp;
using UnityEngine.Video;
using UnityEngine.UI;
using System.IO;
using System.Collections;

/// this class serves as an example on how to configure playback in Unity with VLC for Unity using LibVLCSharp.
/// for libvlcsharp usage documentation, please visit https://code.videolan.org/videolan/LibVLCSharp/-/blob/master/docs/home.md
public class PortraitVideosStretch : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    Texture2D tex = null;
    bool playing;

    public string brokenVideoName;
    public string goodVideoName;
    public string goodPortraitVideoName;
    public Text statusMessage;



    void Awake()
    {
        Core.Initialize(Application.dataPath);

        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        //_libVLC.Log += (s, e) => UnityEngine.Debug.Log(e.FormattedLog); // enable this for logs in the editor

        _mediaPlayer = new MediaPlayer(_libVLC);

        StartCoroutine(TestCOR()); //Start the test
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    void Update()
    {
        //Get size every frame
        uint i_videoHeight = 0;
        uint i_videoWidth = 0;
        _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);

        //Automatically resize output texture (tex) if Size changes
        if (tex == null || tex.width != i_videoWidth || tex.height != i_videoHeight)
        {
            ResizeOutputTexture(i_videoWidth, i_videoHeight);
            ResizeQuadToVideoSize(i_videoWidth, i_videoHeight);
        }
        else if (tex != null)
        {
            //Update the output texture (tex)
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (updated)
            {
                tex.UpdateExternalTexture(texptr);
            }
        }
    }

    //Play a video
    public void Play(string path)
    {
        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        _mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        _mediaPlayer.Play();

    }

    //Get a path to a video in the StreamingAssets folder
    string GetStreamingAssetsPath(string filename)
    {
        return Application.streamingAssetsPath + "/" + filename;

    }

    //This resizes the output texture (tex) to the size VLC thinks the video is
    void ResizeOutputTexture(uint px, uint py)
    {
        var texptr = _mediaPlayer.GetTexture(px, py, out bool updated);
        if (px != 0 && py != 0 && updated && texptr != IntPtr.Zero)
        {
            Debug.Log("Creating texture with height " + py + " and width " + px);
            tex = Texture2D.CreateExternalTexture((int)px,
                (int)py,
                TextureFormat.RGBA32,
                false,
                true,
                texptr);
            GetComponent<Renderer>().material.mainTexture = tex;
        }
    }

    //This stretches the 3d output quad to the same aspect ratio as the output texture (tex)
    //Without this, the output texture will be stretched into a square, which hides the bug
    void ResizeQuadToVideoSize(uint px, uint py)
	{
        var height = 1;
        var width = height * ((float)px / (float)py);
        if(width > 0 && height > 0)
           transform.localScale = new Vector3(width, -height, 1);
	}

    //Sets the status message and logs to the console
    void ShowMessage(string message)
	{
        Debug.Log(message);
        statusMessage.text = message;
	}

    //Play our videos one at a time every 4 seconds
    IEnumerator TestCOR()
	{
		for (int i = 0; i < 500; i++)
		{
            //Play the broken one
			ShowMessage("This video should be in portrait mode, but VLC gets the size wrong, maybe because of the orientation. \n\n Size: 960w by 720h \n\n Size Should Be: 720w by 960h");
			Play(GetStreamingAssetsPath(brokenVideoName));
            yield return new WaitForSeconds(4);

            //Play the good one
            ShowMessage("This landscape video plays perfectly. \n\n Size: 640w by 360h");
            Play(GetStreamingAssetsPath(goodVideoName));
            yield return new WaitForSeconds(4);

            //Play the broken one again
            ShowMessage("About to show what the portrait video should look like.");
            Play(GetStreamingAssetsPath(brokenVideoName));
            yield return new WaitForSeconds(0.5f);
            
            //Stretch it back
            ShowMessage("Stretching the portrait video back to the right aspect ratio isn't nearly as good as not stretching it in the first place, but this is more or less how it should look.");
            ResizeQuadToVideoSize(720, 960);
            yield return new WaitForSeconds(3.5f);

            //Play the other good one
            ShowMessage("This portrait video is fine. This bug probably has to do with the Orientation property, rather than simply with being taller than it is wide. \n\n Size: 500w by 1000h");
            Play(GetStreamingAssetsPath(goodPortraitVideoName));
            yield return new WaitForSeconds(4);

        }
	}



    
}
