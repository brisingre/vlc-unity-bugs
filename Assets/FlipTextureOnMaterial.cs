using UnityEngine;

public class FlipTextureOnMaterial : MonoBehaviour
{
    public bool FlipX;
    public bool FlipY;

    void Awake()
    {
        FlipTexture(FlipX, FlipY);
    }

	private void Update()
	{
        FlipTexture(FlipX, FlipY); //This is just for testing, it's probably fine to just do it in awake in real life
    }

	public void FlipTexture(bool flipX, bool flipY)
	{
        GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(flipX ? -1 : 1, flipY ? -1 : 1);
    }
}
