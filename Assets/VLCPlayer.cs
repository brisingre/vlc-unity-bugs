#if !UMP
using UnityEngine;
using System;
using LibVLCSharp;
using System.Text;

//This is the MediaPlayer wrapper from TV Simulator
public class VLCPlayer : MonoBehaviour
{

	LibVLC LibVLC;
	MediaPlayer Player;
	public Texture2D VlcTexture = null;

	public bool LogToConsole = false;
	public bool RecreatePlayerEveryTime = false;
	public bool PlayOnStart = false;

	
	public string Path = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
	public long Length { get { return Player.Length; } }
	public long Time { get { return Player.Time; } set { Player.SetTime(value, true); } }
	public int Volume { get { return Player.Volume; } set { if(Player != null) Player.SetVolume(value); } }
	public bool IsPlaying { get { return Player.IsPlaying; } }


	public Action OnEndReached;

	public Renderer screen;


	Vector2 Size
	{
		get
		{
			uint width = 0;
			uint height = 0;
			Player.Size(0, ref width, ref height);
			var tracks = Player?.Tracks(TrackType.Video);
				

			if(tracks == null || tracks.Count == 0)
				return new Vector2((int)width, (int)height);

			var orientation = tracks[0]?.Data.Video.Orientation;

			if (orientation == null)
				return new Vector2((int)width, (int)height);
			
			switch (orientation.Value)
			{
				case VideoOrientation.TopLeft:
					return new Vector2((int)width, (int)height);
				case VideoOrientation.BottomRight:
					return new Vector2((int)height, (int)width);
				case VideoOrientation.RightTop:
					return new Vector2((int)height, (int)width);
				default:
					Debug.Log("Orientation Might Be Wrong: " + orientation.ToString());
					return new Vector2((int)width, (int)height);

			}
		}
	}


	#region tracks
	public MediaTrackList VideoTracks { get { return Player.Tracks(TrackType.Video); } }
	public MediaTrackList AudioTracks { get { return Player.Tracks(TrackType.Audio); } }
	public MediaTrackList SubtitleTracks { get { return Player.Tracks(TrackType.Text); } }


	public void SetVideoTrack(MediaTrack track)
	{
		if (track != null)
			Player.Select(track);
		else
			Player.Unselect(TrackType.Video);
	}


	public void SetAudioTrack(MediaTrack track)
	{
		if (track != null)
			Player.Select(track);
		else
			Player.Unselect(TrackType.Audio);
	}


	public void SetSubtitleTrack(MediaTrack track)
	{
		if (track != null)
			Player.Select(track);
		else
			Player.Unselect(TrackType.Text);
	}
	#endregion

	#region unity
	void Awake()
	{
		Core.Initialize(Application.dataPath);

		//_libVLC = new LibVLC(enableDebugLogs: true, "--no-osd", "--video-filter=transform", "--transform-type=vflip");
		LibVLC = new LibVLC(enableDebugLogs: true, "--no-osd");

		Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
		//if(PPrefs.GetInt("LogVlcToConsole") > 0)
		//	LibVLC.Log += (s, e) => UnityEngine.Debug.Log(e.FormattedLog); // enable this for logs in the editor
		CreatePlayer();

		if (PlayOnStart)
			Play();
	}

	void OnDisable()
	{
		SafeDestroyPlayer();

		LibVLC?.Dispose();
		LibVLC = null;
	}

	void Update()
	{
		//if (!playing) return;

		UpdateTexture();
		if (screen != null)
			screen.material.mainTexture = Texture;

	}
	#endregion

	public void TryOnEndReached()
	{
		OnEndReached();
	}

	#region player
	void CreatePlayer()
	{
		Debug.Log("[VLC] CreatePlayer()");
		if (Player != null)
			SafeDestroyPlayer();

		Player = new MediaPlayer(LibVLC);

		Player.EnableHardwareDecoding = true;


		Player.SeekableChanged += (a, b) => {
			if (targetTime > 0 && Player.IsSeekable)
			{
				Time = targetTime;
				targetTime = -2;
			}
		};
		
		Player.EndReached += (a, b) => {
			try
			{
				OnEndReached();
			}
			catch (Exception e)
			{
				Debug.LogError("Caught Exception: " + e.ToString());
			}
		};
	}


	public void DestroyPlayer()
	{
		Debug.Log("[VLC] DestroyPlayer()");
		Player?.Stop(); //This is the trouble maker
		Player?.Dispose();
		Player = null;
	}
	public void SafeDestroyPlayer()
	{
		Debug.Log("[VLC] DestroyPlayer()");
		//Player?.Stop();
		Player?.Dispose();
		Player = null;
	}
	#endregion


	public RenderTexture Texture;
	#region texture
	void UpdateTexture()
	{
		var texSize = Size;
		if (VlcTexture == null || VlcTexture.width != texSize.x || VlcTexture.height != texSize.y)
		{
			RecreateTexture(texSize);
		}
		else if (VlcTexture != null)
		{
			var texptr = Player.GetTexture((uint)VlcTexture.width, (uint)VlcTexture.height, out bool updated);//?
			if (updated)
			{
				VlcTexture.UpdateExternalTexture(texptr);
				Graphics.Blit(VlcTexture, Texture, new Vector2(1, -1), Vector2.zero);
			}
		}
	}

	void RecreateTexture(Vector2 texSize)
	{
		// If received size is not null, it and scale the texture
		var texptr = Player.GetTexture((uint)texSize.x, (uint)texSize.y, out bool updated);


		if (texSize.x != 0 && texSize.y != 0 && updated && texptr != IntPtr.Zero)
		{
			Debug.Log("Creating texture with height " + texSize.y + " and width " + texSize.x);

			if (VlcTexture != null)
				DestroyImmediate(VlcTexture);

			VlcTexture = Texture2D.CreateExternalTexture(
				(int)texSize.x,
				(int)texSize.y,
				TextureFormat.RGBA32,
				false,
				true,
				texptr);

			if (Texture != null)
				DestroyImmediate(Texture);

			Texture = new RenderTexture(VlcTexture.width, VlcTexture.height, 32, RenderTextureFormat.ARGB32);

			GetComponent<Renderer>().material.mainTexture = Texture;
		}
	}
	#endregion

	#region play/pause
	public void PlayPause()
	{
		Debug.Log ("[VLC] PlayPause()");

		if (Player.IsPlaying)
		{
			Pause();
		}
		else
		{
			Play();
		}
	}

	public void Play()
	{
		Debug.Log("[VLC] Play()");
		Play(Path);
	}

	public void Play(string path)
	{
		Debug.Log("[VLC] Play("+path+")");

		if(RecreatePlayerEveryTime)
			CreatePlayer();

		Path = path;

		var uri = new Uri(path);
		if (Player.Media == null)
		{
			SetMedia(uri);
		}
		else
		{
			Debug.Log("URI = " + uri.AbsoluteUri);
			if (Player.Media.Mrl != uri.AbsoluteUri)
			{
				Debug.Log("MRL = " + Player.Media.Mrl);
				SetMedia(uri);

			}
		}
		Debug.Log("Path = " + path);
		Player.Play();

	}

	public void SetMedia(Uri uri)
	{

		Player.Media?.Dispose();
		
		Player.Media = new Media(LibVLC, uri);
	}

	public void Pause()
	{
		Debug.Log("[VLC] Pause()");
		Player.Pause();
	}

	public void Stop()
	{
		Debug.Log ("[VLC] Stop()");

		Player?.Stop();
		
		//GetComponent<Renderer>().material.mainTexture = null;
		//VlcTexture = null;
	}

	public void Seek(long delta)
	{
		Debug.Log("[VLC] Seek(" + delta + ")");
		Player.SetTime(Player.Time + delta);
	}
	#endregion

	public long targetTime;

	public void SetTime(long time)
	{ 
		if(Player.IsSeekable)
		{
			Time = time;
			targetTime = -1;
		}
		else
		{
			targetTime = time;
		}
	}

}
#endif