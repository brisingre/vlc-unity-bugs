using UnityEngine;
using System;
using LibVLCSharp;

public class AudioPlaybackDestroyPlayerFreeze : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    const int seekTimeDelta = 5000;
    Texture2D tex = null;
    bool playing;

    void Awake()
    {
        Core.Initialize(Application.dataPath);

        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");

        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        _libVLC.Log += (s, e) => UnityEngine.Debug.Log(e.FormattedLog); //Enabled in this demo (not in MinimalPlayback.cs)

        //Added to MinimalPlayback.cs
        _mediaPlayer = new MediaPlayer(_libVLC);
        //Added to MinimalPlayback.cs
        _mediaPlayer.SetAudioCallbacks(
            audioPlayCallback,
            audioPauseCallback,
            audioResumeCallback,
            audioFlushCallback, //SET THIS TO null TO WORKAROUND THE BUG
            audioDrainCallback);



        PlayPause();
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    public void PlayPause()
    {
        Debug.Log("[VLC] Toggling Play Pause !");
        if (_mediaPlayer == null)
        {
            _mediaPlayer = new MediaPlayer(_libVLC);
        }
        if (_mediaPlayer.IsPlaying)
        {
            _mediaPlayer.Pause();
        }
        else
        {
            playing = true;

            if (_mediaPlayer.Media == null)
            {
                // playing remote media
                _mediaPlayer.Media = new Media(_libVLC, new Uri("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"));
            }

            _mediaPlayer.Play();
        }
    }

    public void Stop()
    {
        Debug.Log("[VLC] Stopping Player !");

        playing = false;
        _mediaPlayer?.Stop();

        // there is no need to dispose every time you stop, but you should do so when you're done using the mediaplayer and this is how:
        // _mediaPlayer?.Dispose(); 
        // _mediaPlayer = null;
        GetComponent<Renderer>().material.mainTexture = null;
        tex = null;
    }

    //Added to MinimalPlayback.cs
    public void DestroyPlayer()
	{
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;
    }

    //Added to MinimalPlayback.cs
    public void StopAndDestroyPlayer()
	{
        Stop();
        DestroyPlayer();
	}

    void Update()
    {
        if (!playing) return;

        if (tex == null)
        {
            // If received size is not null, it and scale the texture
            uint i_videoHeight = 0;
            uint i_videoWidth = 0;

            _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (i_videoWidth != 0 && i_videoHeight != 0 && updated && texptr != IntPtr.Zero)
            {
                Debug.Log("Creating texture with height " + i_videoHeight + " and width " + i_videoWidth);
                tex = Texture2D.CreateExternalTexture((int)i_videoWidth,
                    (int)i_videoHeight,
                    TextureFormat.RGBA32,
                    false,
                    true,
                    texptr);
                GetComponent<Renderer>().material.mainTexture = tex;
            }
        }
        else if (tex != null)
        {
            var texptr = _mediaPlayer.GetTexture((uint)tex.width, (uint)tex.height, out bool updated);
            if (updated)
            {
                tex.UpdateExternalTexture(texptr);
            }
        }
    }


    //Added to MinimalPlayback.cs (all functions empty)
    #region audioCallbacks
    public MediaPlayer.LibVLCAudioPlayCb audioPlayCallback = (data, samples, count, pts) => {

    };

    public MediaPlayer.LibVLCAudioPauseCb audioPauseCallback = (data, pts) => {

    };

    public MediaPlayer.LibVLCAudioResumeCb audioResumeCallback = (data, pts) => {

    };

    public MediaPlayer.LibVLCAudioFlushCb audioFlushCallback = (data, pts) => {

    };

    public MediaPlayer.LibVLCAudioDrainCb audioDrainCallback = (data) => {

    };
    #endregion
}
