using UnityEngine;
using System;
using LibVLCSharp;

public class DoublePlayFreeze : MonoBehaviour
{
    LibVLC _libVLC;
    MediaPlayer _mediaPlayer;
    Texture2D _vlcTex = null; //This is the texture libVLC writes to directly. It's private.

    
    public RenderTexture texture = null; //We copy it into this texture which we actually use in unity. It's public.

    public Renderer screen;

    public string path = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";

    public bool enableLogging = true;


    void Awake()
    {
        //Setup LibVLC
        Core.Initialize(Application.dataPath);
        _libVLC = new LibVLC(enableDebugLogs: true, "--no-osd");


        //Setup Error Logging
        Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
        _libVLC.Log += (s, e) =>
        {
            if(enableLogging)
                Debug.Log(e.FormattedLog); 
        };

        //Setup Screen
        if (screen == null)
            screen = GetComponent<Renderer>();

        //Setup Media Player
        CreateMediaPlayer();
    }

    void OnDisable()
    {
        _mediaPlayer?.Stop();
        _mediaPlayer?.Dispose();
        _mediaPlayer = null;

        _libVLC?.Dispose();
        _libVLC = null;
    }

    void Update()
    {
        //Get size every frame
        uint i_videoHeight = 0;
        uint i_videoWidth = 0;
        _mediaPlayer.Size(0, ref i_videoWidth, ref i_videoHeight);

        //Automatically resize output textures if size changes
        if (_vlcTex == null || _vlcTex.width != i_videoWidth || _vlcTex.height != i_videoHeight)
        {
            ResizeOutputTextures(i_videoWidth, i_videoHeight);
        }


        if (_vlcTex != null)
        {
            //Update the vlc texture (tex)
            var texptr = _mediaPlayer.GetTexture(i_videoWidth, i_videoHeight, out bool updated);
            if (updated)
            {
                _vlcTex.UpdateExternalTexture(texptr);
                //When we support a second platform, this will have to change a little
                Graphics.Blit(_vlcTex, texture, new Vector2(-1, 1), Vector2.zero); //Copy the vlc texture into the output texture, flipped over
            }
        }
    }


	#region internal
	//This makes a new MediaPlayer object and disposes of the old one
	void CreateMediaPlayer()
    {
        if(_mediaPlayer != null)
		{
            _mediaPlayer?.Dispose();
            _mediaPlayer = null;
        }
        _mediaPlayer = new MediaPlayer(_libVLC);

    }

    //This resizes the output textures to the size VLC thinks the video is
    void ResizeOutputTextures(uint px, uint py)
    {
        var texptr = _mediaPlayer.GetTexture(px, py, out bool updated);
        if (px != 0 && py != 0 && updated && texptr != IntPtr.Zero)
        {
            //If the currently playing video uses the Bottom Right orientation, we have to do this to avoid stretching it.
            if(GetVideoOrientation() == VideoOrientation.BottomRight)
            {
                uint swap = px;
                px = py;
                py = swap;
			}

            _vlcTex = Texture2D.CreateExternalTexture((int)px, (int)py, TextureFormat.RGBA32, false, true, texptr); //Make a texture of the proper size for the video to output to
            texture = new RenderTexture(_vlcTex.width, _vlcTex.height, 0, RenderTextureFormat.ARGB32); //Make a renderTexture the same size as vlctex

            if(screen != null)
                screen.material.mainTexture = texture;
        }
    }

    //This returns the video orientation for the currently playing video
    VideoOrientation? GetVideoOrientation()
	{
        var tracks = _mediaPlayer?.Tracks(TrackType.Video);

        if (tracks == null || tracks.Count == 0)
            return null;

        var orientation = tracks[0]?.Data.Video.Orientation; //At the moment we're assuming the track we're playing is the first track

        return orientation;
    }
	#endregion

	#region tests
    public void PlayWithNewPlayer()
	{
        CreateMediaPlayer();

        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        _mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        _mediaPlayer.Play();
    }

    public void PlayWithNewMedia()
	{
        if (_mediaPlayer.Media != null)
            _mediaPlayer.Media.Dispose();

        //_mediaPlayer.Media = new Media(_libVLC, new Uri(path));

        // _mediaPlayer.Play();
        _mediaPlayer.Play(new Media(_libVLC, new Uri(path)));
    }


    public void JustPlay()
    {
        _mediaPlayer.Play();
    }

    public void StopThenPlay()
	{
        _mediaPlayer.Stop();
        _mediaPlayer.Play();
    }

    public void PauseThenPlay()
    {
        _mediaPlayer.Pause();
        _mediaPlayer.Play();
    }

    public void mfklTest()
    {
        _mediaPlayer.Play(new Media(_libVLC, new Uri("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")));
    }
    #endregion
}
